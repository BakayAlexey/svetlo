import React, { Component, Fragment } from 'react';
import { Switch, Route, NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import ProductList from './components/admin/products/product-list';
import ProductAdd from './components/admin/products/product-add';
import ProductEdit from './components/admin/products/products-edit';
import FaqList from './components/admin/faq/faq-list';
import FaqAdd from './components/admin/faq/faq-add';
import FaqEdit from './components/admin/faq/faq-edit';
import { isAuthorizedSelector, signOut } from './ducks/auth';
import SignIn from './components/admin/sign-in';
import './admin.scss';
import './components/admin/list.scss';
import './components/admin/img-list.scss';

class Admin extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }

  render() {
    const { match: { path }, user, signOutAction } = this.props;

    if (!user) {
      return (
        <SignIn />
      );
    }

    return (
      <Fragment>
        <div className="container">
          <nav className="admin-nav">
            <ul className="admin-nav-list">
              <li>
                <NavLink to="/admin/products">
                  Products
                </NavLink>
              </li>
              <li>
                <NavLink to="/admin/reviews">
                  Reviews
                </NavLink>
              </li>
              <li>
                <NavLink to="/admin/faq">
                  FAQ
                </NavLink>
              </li>
            </ul>
            <button
              type="button"
              className="admin-log-out"
              onClick={signOutAction}
            >
              Log out
            </button>
          </nav>
        </div>
        <Switch>
          <Route path={`${path}/products/add`} component={ProductAdd} />
          <Route path={`${path}/products/:id`} component={ProductEdit} />
          <Route path={`${path}/products`} component={ProductList} />
          <Route path={`${path}/faq/add`} component={FaqAdd} />
          <Route path={`${path}/faq/:id`} component={FaqEdit} />
          <Route path={`${path}/faq`} component={FaqList} />
        </Switch>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  user: isAuthorizedSelector(state),
});

export default connect(mapStateToProps, { signOutAction: signOut })(Admin);
