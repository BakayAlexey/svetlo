import React, { Fragment } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import Header from './components/header';
import Footer from './components/footer';
import Home from './components/routes/app/home';
import About from './components/routes/app/about';
import Materials from './components/routes/app/materials';
import Products from './components/routes/app/products';
import ProductsSingle from './components/routes/app/products-single';
import Services from './components/routes/app/services';
import Contacts from './components/routes/app/contacts';

const appNavs = [
  {
    id: 0,
    to: '/about',
    descr: 'О нас',
  },
  {
    id: 1,
    to: '/materials',
    descr: 'Материалы',
  },
  {
    id: 2,
    to: '/products',
    descr: 'Товары',
  },
  {
    id: 3,
    to: '/services',
    descr: 'Электрика',
  },
  {
    id: 4,
    to: '/contacts',
    descr: 'Контакты',
  },
];

const App = () => (
  <Fragment>
    <Header navs={appNavs} />
    <Switch>
      <Route path="/" component={Home} exact />
      <Route path="/about" component={About} />
      <Route path="/materials" component={Materials} />
      <Route path="/products/:id" component={ProductsSingle} />
      <Route path="/products" component={Products} />
      <Route path="/services" component={Services} />
      <Route path="/contacts" component={Contacts} />
      <Route path="/admin" component={Contacts} />
      <Redirect to="/" />
    </Switch>
    <Footer />
  </Fragment>
);

export default App;
