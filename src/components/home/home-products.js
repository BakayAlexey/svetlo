import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import ProductList from '../products-list/products-list';
import {
  loadAllProducts,
  productsItemsSelector,
  productsLoadingSelector,
  productsLoadedSelector,
} from '../../ducks/products';

class HomeProducts extends Component {
  componentDidMount() {
    const { loading, loaded, loadAll } = this.props;
    if (!loaded && !loading) {
      loadAll();
    }
  }

  render() {
    const { productsData } = this.props;

    if (!productsData || productsData.length === 0) {
      return null;
    }

    const productsDataHome = productsData.sort((a, b) => {
      if (+a.order > +b.order) return 1;
      if (+a.order < +b.order) return -1;
      return 0;
    }).slice(0, 3);

    return (
      <div className="products-wrapper products-main block-wrapper">
        <div className="products-wrap block-wrap">
          <div className="container">
            <div className="products block">
              <h2 className="products-title title">
                <span>Самые популярные </span>
                светильники:
              </h2>

              <ProductList productsData={productsDataHome} />

              <div className="products-btn-wrapper">
                <Link to="/products" className="products-btn btn">
                  в магазин
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  productsData: productsItemsSelector(state),
  loading: productsLoadingSelector(state),
  loaded: productsLoadedSelector(state),
});

const mapDispatchToProps = {
  loadAll: loadAllProducts,
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeProducts);
