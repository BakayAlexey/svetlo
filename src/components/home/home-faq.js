import React, { Component } from 'react';
import { connect } from 'react-redux';
import './home-faq.scss';
import {
  faqItemsSelector,
  faqLoadedSelector,
  faqLoadingSelector,
  loadAllFaq,
} from '../../ducks/faq';

const HomeFaqItem = (props) => {
  const {
    data: { question, answer },
    style,
    isOpen,
    toggle,
  } = props;

  return (
    <div
      className={isOpen ? 'faq-item faq-item--invert' : 'faq-item'}
      onClick={toggle}
    >
      <div className="faq-item-inner-wrapper">
        <div
          className="faq-item-question title-section"
          style={style}
        >
          {question}
        </div>
        <div className="faq-item-answer text-main">
          {answer}
        </div>
      </div>
    </div>
  );
};

class HomeFaq extends Component {
  state = {
    openItemId: null,
  };

  componentDidMount() {
    const { loading, loaded, loadAllFaqAction } = this.props;
    if (!loading && !loaded) {
      loadAllFaqAction();
    }
  }


  toggleItem = currentItemId => () => {
    const { openItemId } = this.state;
    this.setState({
      openItemId: (currentItemId === openItemId) ? null : currentItemId,
    });
  };

  render() {
    const { openItemId } = this.state;
    const { faqItems } = this.props;

    const faqItemsSorted = faqItems.sort((a, b) => {
      if (+a.order > +b.order) return 1;
      if (+a.order < +b.order) return -1;
      return 0;
    }).slice(0, 5);

    return (
      <div
        className="faq-wrapper block-wrapper"
        style={{ backgroundImage: `url("${process.env.PUBLIC_URL}/img/faq-bg.jpg")` }}
      >
        <div className="faq-wrap block-wrap">
          <div className="container">
            <div className="faq block">
              <div className="faq-items">
                <h2 className="faq-title title">
                  <span>FAQ</span>
                </h2>
                {faqItemsSorted.map((item, index) => {
                  let style;
                  if (index === 0) {
                    style = {
                      backgroundImage: `url("${process.env.PUBLIC_URL}/img/faq-img1.png")`,
                    };
                  } else if (index === 2) {
                    style = {
                      backgroundImage: `url("${process.env.PUBLIC_URL}/img/faq-img2.png")`,
                    };
                  } else if (index === 3) {
                    style = {
                      backgroundImage: `url("${process.env.PUBLIC_URL}/img/faq-img3.png")`,
                    };
                  } else {
                    style = null;
                  }

                  return (
                    <HomeFaqItem
                      key={item.id}
                      data={item}
                      isOpen={openItemId === item.id}
                      toggle={this.toggleItem(item.id)}
                      style={style}
                    />
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  faqItems: faqItemsSelector(state),
  loading: faqLoadingSelector(state),
  loaded: faqLoadedSelector(state),
});

const mapDispatchToProps = {
  loadAllFaqAction: loadAllFaq,
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeFaq);
