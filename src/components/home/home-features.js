import React from 'react';
import './home-features.scss';

const HomeFeatures = () => (
  <div className="features-wrapper">
    <div className="container">
      <div className="features block-wrapper">
        <div className="features-img1 wow fadeInLeftBig">
          <img
            src={`${process.env.PUBLIC_URL}/img/features-img1.png`}
            alt="img"
          />
        </div>

        <div className="features-img2 wow fadeInRightBig">
          <img
            src={`${process.env.PUBLIC_URL}/img/features-img2.png`}
            alt="img"
          />
        </div>

        <div className="features-inner-wrapper block-wrap">
          <div className="features-descr">
            <h2 className="features-descr-title title">
              Почему
              <span> наши светильники </span>
              так хороши?
            </h2>

            <div className="features-descr-text text-main">
              <p>
                При изготовлении каждого изделия материалом выступает только качественная и
                натуральная древесина. Для работы используется в основном цельное дерево. Это самое
                <span className="text-bold"> экологическое и долговечное сырье</span>
                , которое несет свое тепло и красоту.
                <a href="products.html" className="text-link text-bold"> Светильники </a>
                из дерева придают любому помещению уют и комфорт, даря свой мягкий свет.
              </p>
              <p>
                Каждый
                <span className="text-bold"> светильник создается вручную</span>
                , с душой, нашими мастерами, которые уже долгие годы работают с деревом. Все
                деревянные светильники имеют крепкую конструкцию как самого корпуса, так и каждой
                детали. Они изготовлены с соблюдением всех норм и требований, а уникальный дизайн
                подойдет настоящим ценителям прекрасного.
              </p>
            </div>
          </div>

          <div className="features-reason">
            <div className="features-reason-inner-wrapper">
              <div className="features-reason-bg" />
              <h2 className="features-reason-title title">
                <span>7 причин </span>
                выбрать наши светильники
              </h2>

              <ul className="features-reason-items">
                <li className="features-reason-item">
                  <div className="features-reason-item-num elem-underline">01</div>
                  <div className="features-reason-item-text text-main">
                    Многолетний опыт работы
                  </div>
                </li>

                <li className="features-reason-item">
                  <div className="features-reason-item-num elem-underline">02</div>
                  <div className="features-reason-item-text text-main">
                    Качественное и быстрое выполнение заданий без посредников
                  </div>
                </li>

                <li className="features-reason-item">
                  <div className="features-reason-item-num elem-underline">03</div>
                  <div className="features-reason-item-text text-main">
                    Реализация проектов по Вашим индивидуальным требованиям
                  </div>
                </li>

                <li className="features-reason-item">
                  <div className="features-reason-item-num elem-underline">04</div>
                  <div className="features-reason-item-text text-main">
                    Оплата за готовый результат
                  </div>
                </li>

                <li className="features-reason-item">
                  <div className="features-reason-item-num elem-underline">05</div>
                  <div className="features-reason-item-text text-main">
                    Предоставление гарантии на 12 месяцев
                  </div>
                </li>

                <li className="features-reason-item">
                  <div className="features-reason-item-num elem-underline">06</div>
                  <div className="features-reason-item-text text-main">
                    Соответствие освещения нормам международного стандарта по освещению ISO 8995
                  </div>
                </li>

                <li className="features-reason-item">
                  <div className="features-reason-item-num elem-underline">07</div>
                  <div className="features-reason-item-text text-main">
                    Скидка на установку светильника – 50%
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default HomeFeatures;
