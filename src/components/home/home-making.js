import React from 'react';
import './home-making.scss';

const HomeMaking = () => (
  <div
    className="making-wrapper block-wrapper"
    style={{ backgroundImage: `url(${process.env.PUBLIC_URL}/img/making-bg.jpg)` }}
  >
    <div className="making-wrap block-wrap">
      <div className="container">
        <div className="making block">
          <div className="making-bg" />

          <h2 className="making-title title">
            <span>Как мы делаем </span>
            наши светильники:
          </h2>

          <div className="making-content">
            <div className="making-content-video-wrapper">
              <div className="making-content-video">
                <iframe
                  title="video"
                  src="https://www.youtube.com/embed/sSSdZYuvzpA"
                  frameBorder="0"
                  allowFullScreen
                />
              </div>
            </div>

            <div className="making-content-descr">
              <div className="making-content-descr-text text-main">
                <p>
                  Как мы создаём наши произведения? Ответ прост: вручную, с любовью и частичкой
                  своей души. Каждая модель разработана индивидуально, имеет свои особенности и
                  уникальность.
                </p>
                <p>
                  Объединяет наше творчество материал, из которого изготовляются светильники. Это
                  дерево – натуральное, надёжное и, что не мало важно, красивое сырьё. Ко всему
                  прочему, на сегодня в тренде сочетание природных и эстетичных элементов
                  интерьера.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default HomeMaking;
