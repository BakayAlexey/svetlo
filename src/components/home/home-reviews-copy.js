import React from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.scss';
import 'slick-carousel/slick/slick-theme.scss';
import './home-reviews.scss';

const HomeReviews = () => {
  const setting = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    className: 'reviews-items',
  };

  return (
    <div className="feedback-reviews-block">
      <div className="reviews-wrapper">
        <div className="container">
          <div className="reviews">
            <div className="reviews-title title">
              <span>Отзывы </span>
              наших клиентов
            </div>

            <div className="reviews-items-wrapper">
              <Slider {...setting}>
                <div className="reviews-item">
                  <div className="reviews-item-img">
                    <img src={`${process.env.PUBLIC_URL}/img/testimonials_photo_1.jpg`} alt="img" />
                  </div>

                  <div className="reviews-item-content">
                    <div className="reviews-item-content-name title-section">
                      Наташа
                    </div>
                    <div className="reviews-item-content-text text-main">
                      Огромное спасибо за такую красоту! Всегда была фанаткой изделий из пластика и
                      стекла, но попробовав светильники из дерева поняла разницу. Смотрится просто
                      восхитительно, и все натурально и экологически чисто. Советую всем!
                    </div>
                  </div>
                </div>

                <div className="reviews-item">
                  <div className="reviews-item-img">
                    <img src={`${process.env.PUBLIC_URL}/img/testimonials_photo_2.jpg`} alt="img" />
                  </div>

                  <div className="reviews-item-content">
                    <div className="reviews-item-content-name title-section">
                      Людмила
                    </div>
                    <div className="reviews-item-content-text text-main">
                      Выражаю благодарность “Светло” за сотрудничество и прекрасную работу. Уже
                      более года использую светильники из древесины в дизайне интерьеров для
                      клиентов. Светильники выглядят очень органично и стильно во многих вариантах,
                      а качество исполнения отличное. Спасибо
                    </div>
                  </div>
                </div>

                <div className="reviews-item">
                  <div className="reviews-item-img">
                    <img src={`${process.env.PUBLIC_URL}/img/testimonials_photo_3.jpg`} alt="img" />
                  </div>

                  <div className="reviews-item-content">
                    <div className="reviews-item-content-name title-section">
                      Инна
                    </div>
                    <div className="reviews-item-content-text text-main">
                      Заказывали у ребят 3 десятка светильников для своей кофейни. Хочу похвалить -
                      интерьер действительно получился с ними более полный, стало очень уютненько и
                      тепло. Очень классно, что такой широкий выбор древесины и можно подобрать
                      именно то, что нужно. Всем остались довольны!
                    </div>
                  </div>
                </div>

                <div className="reviews-item">
                  <div className="reviews-item-img">
                    <img src={`${process.env.PUBLIC_URL}/img/testimonials_photo_4.jpg`} alt="img" />
                  </div>

                  <div className="reviews-item-content">
                    <div className="reviews-item-content-name title-section">
                      Тамара
                    </div>
                    <div className="reviews-item-content-text text-main">
                      Просто обожаю качественные товары ручной работы из натуральных материалов.
                      Поэтому не смогла пройти мимо этих светильников. Извините за то, что так долго
                      мучала вас тем, что никак не могла выбрать. Но такие все красивые модели, что
                      меня можно понять! В общем, спасибо за то, что среди кучи магазинов, которые
                      торгуют конвейерными товарами - вы продаете что-то действительно настоящее.
                    </div>
                  </div>
                </div>
              </Slider>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HomeReviews;
