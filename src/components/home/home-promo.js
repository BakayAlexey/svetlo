import React from 'react';
import { Link } from 'react-router-dom';
import './home-promo.scss';


const HomePromo = () => (
  <div
    className="promo-wrapper block-wrapper"
    style={{
      backgroundImage: `url(${process.env.PUBLIC_URL}/img/promo-bg.jpg)`,
    }}
  >
    <div className="promo-wrap block-wrap">
      <div className="container">
        <div className="promo block">

          <div className="promo-phrase">
            <div className="promo-phrase-bg" />
            <h1 className="promo-phrase-text title-big">
              <span>Светильники </span>
              из дерева
              <span> ручной работы</span>
            </h1>
          </div>

          <div className="promo-info">
            <h2 className="promo-info-text title">
              <span>Дизайнерские светильники </span>
              и люстры из натуральной древесины
              <span> ручной работы</span>
            </h2>
            <div className="promo-info-btn-wrapper">
              <Link to="/products" className="promo-info-btn btn">
                Узнать больше
              </Link>
            </div>
          </div>

          <div className="promo-bg">
            <img src={`${process.env.PUBLIC_URL}/img/main_lamp_img.png`} alt="img" />
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default HomePromo;
