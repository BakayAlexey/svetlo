import React from 'react';
import './services-promo.scss';

const ServicesPromo = () => (
  <div
    className="services-promo-wrapper block-wrapper"
    style={{ backgroundImage: `url(${process.env.PUBLIC_URL}/img/services_background_1.jpg)` }}
  >
    <div className="services-promo-wrap block-wrap">
      <div className="container">
        <div className="services-promo">
          <div className="services-promo-content">
            <h2 className="services-promo-title title-big">
              <span>Правильное электричество</span>
              <br />
              для вашего дома
            </h2>

            <div className="services-promo-text text-main">
              <p>
                Организация электропитания – это важная составляющая любого помещения. Без
                электроснабжения не будет работать ни освещение, ни приборы. Важным критерием в
                данном вопросе является правильная проектировка и электромонтаж – с учётом всех
                потребностей, безопасности и дизайна помещения.
              </p>
              <p>
                Проектировку и монтаж электропитания лучше доверить профессионалам, так как
                допущенные ошибки могут иметь негативные последствия в будущем (невозможностью
                установить или использовать технику там, где Вы хотите, внезапное отсутствие
                электропитания, угроза здоровью и жизни человека, дополнительные материальные
                затраты).
              </p>
            </div>
          </div>

          <div className="services-promo-img">
            <img src={`${process.env.PUBLIC_URL}/img/services-promo-img.png`} alt="img" />
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default ServicesPromo;
