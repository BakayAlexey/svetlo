import React from 'react';
import './services-process.scss';

const ServicesProcess = () => (
  <div
    className="process-wrapper block-wrapper"
    style={{ backgroundImage: `url("${process.env.PUBLIC_URL}/img/services_background_2.jpg")` }}
  >
    <div className="process-wrap block-wrap">
      <div className="container">
        <div className="process">
          <div className="process-title title">
            <span>Сроки и процесс работы </span>
            с нами
          </div>

          <div className="process-text text-main">
            <p>
              Перед началом работы по электромонтажу необходимо составить план разметки прокладки
              кабелей, схему освещения, выбрать равномерное и удобное размещение коробок
              распределения для розеток и выключателей. Все это необходимо сделать с расчетом
              расстановки всех электроприборов помещения.
            </p>
            <p>
              В зависимости от задач и требований подбираются способы монтажа, оборудование, кабели,
              электрощиты. После подведения всех итогов клиенту
              <span className="text-bold"> предоставляется смета</span>
              , которая им должна быть утверждена.
            </p>
            <p>
              Далее проводятся все подготовительные работы, происходит заземление приборов и
              оборудования. После этого проводится основная работа по электромонтажу: прокладываются
              кабели и провода, электропроводка, устанавливаются электрощитки и УЗО, подключение
              всего оборудования к сети.
            </p>
            <p>
              <span className="text-bold">По завершению электромонтажа </span>
              проводятся пуско-наладочные работы, проверка качества исполнения с нагрузкой, изучение
              работы всего оборудования, отладка, установка освещения и прочих необходимых
              установок.
            </p>
            <p>
              Цены на монтаж электропроводки и других услуг рассчитываются исходя из объекта и
              потребностей клиента, по прайсу.
            </p>
          </div>

          <div className="process-img wow fadeInRightBig">
            <img src={`${process.env.PUBLIC_URL}/img/process.png`} alt="img" />
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default ServicesProcess;
