import React from 'react';
import './services-info.scss';

const ServicesInfo = () => (
  <div className="info-block services-info">
    <div className="info-block-left-wrapper block-wrapper">

      <div className="info-block-left-wrap block-wrap">
        <div className="container-left">
          <div className="info-block-left">
            <div className="info-block-title title">
              Что мы
              <span> готовы вам предложить</span>
              ?
            </div>

            <div className="info-block-text text-main">
              <p>
                Квалифицированные, опытные специалисты компании «SVETLO» готовы решить любой, даже
                самый сложный вопрос
                <span className="text-bold"> электромонтажа в Киеве</span>
                . В перечень наших услуг входит качественное выполнение таких заданий:
              </p>
            </div>

            <ul className="info-block-list text-main">
              <li>проектирование электропитания;</li>
              <li>комплексный монтаж электропроводки;</li>
              <li>ревизия электропроводки с полной или частичной её заменой;</li>
              <li>установка или замена розеток, выключателей, электрозвонков;</li>
              <li>монтаж элетрощитков, распределительных коробок, стабилизаторов напряжения;</li>
              <li>установка люстр, бра, светильников;</li>
              <li>установка датчиков движения, регуляторов тёплого пола;</li>
              <li>монтаж слаботочных линий - телефон, TV-антенны, Internet;</li>
              <li>
                установка дистанционного управления для включения и выключения светильников, бра,
                люстр, подсветок;
              </li>
              <li>установка и подключение светодиодной (LED) подсветки.</li>
            </ul>
          </div>
        </div>
      </div>
    </div>

    <div className="info-block-right-wrapper block-wrapper">
      <div className="services-info-img2 wow fadeInRightBig">
        <img src={`${process.env.PUBLIC_URL}/img/features.png`} alt="img" />
      </div>

      <div className="info-block-right-wrap block-wrap">
        <div className="container-right">
          <div className="info-block-right">
            <div className="info-block-title title">
              <span>5 причин </span>
              выбрать нас:
            </div>

            <ul className="info-block-num-list text-main">
              <li>
                <span className="num elem-underline">01</span>
                Более 5-ти лет опыта в сфере услуг по электрике.
              </li>

              <li>
                <span className="num elem-underline">02</span>
                Качественная работа в срок без участия посредников.
              </li>

              <li>
                <span className="num elem-underline">03</span>
                Выполнение работ с учетом уникальных потребностей.
              </li>

              <li>
                <span className="num elem-underline">04</span>
                Гарантия на выполненные работы на 3 года.
              </li>

              <li>
                <span className="num elem-underline">05</span>
                Соответствие всем нормам и международным стандартам.
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default ServicesInfo;
