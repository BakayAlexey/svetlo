import React from 'react';
import './materials-requirements.scss';

const MaterialsRequirements = () => (
  <div
    className="requirements-wrapper block-wrapper"
    style={{ backgroundImage: `url(${process.env.PUBLIC_URL}/img/materials_background_2.jpg)` }}
  >
    <div className="requirements-wrap block-wrap">
      <div className="container">
        <div className="requirements">
          <div className="requirements-bg" />

          <h2 className="requirements-title title">
            <span>Требования </span>
            к натуральным светильникам из древесины
          </h2>

          <div className="requirements-content text-main">
            <div className="requirements-items-wrapper">
              <div className="requirements-items-descr">
                Все
                <span className="text-bold"> светильники из натуральных материалов </span>
                должны отвечать определенным требованиям:
              </div>

              <ul className="requirements-items">
                <li className="requirements-item">
                  экологичность древесины и сопутствующих материалов;
                </li>
                <li className="requirements-item">
                  безопасность норм для помещений и здоровья человека;
                </li>
                <li className="requirements-item">
                  иметь крепкую конструкцию корпуса и деталей;
                </li>
                <li className="requirements-item">
                  иметь качественные и надежные электропроводящие элементы.
                </li>
              </ul>

              <div className="requirements-items-img">
                <img src={`${process.env.PUBLIC_URL}img/requirements-img.png`} alt="img" />
              </div>
            </div>

            <div className="requirements-additional">
              <div className="requirements-additional-descr text-bold">
                3 превосходства деревянных изделий над другими:
              </div>

              <ul className="requirements-additional-items">
                <li className="requirements-additional-item">
                  <span className="requirements-additional-item-num elem-underline">01</span>
                  Дерево - часть природы; стихия творчества и символ роста.
                </li>

                <li className="requirements-additional-item">
                  <span className="requirements-additional-item-num elem-underline">02</span>
                  Дерево означает жизнь, живое. Даёт возможность ощутить гармонию с окружающим
                  миром.
                </li>

                <li className="requirements-additional-item">
                  <span className="requirements-additional-item-num elem-underline">03</span>
                  Свет, исходящий из деревянных изделий, однозначно мягкий, уютный, успокаивающий.
                </li>
              </ul>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
);

export default MaterialsRequirements;
