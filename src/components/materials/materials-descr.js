import React from 'react';
import './materials-descr.scss';

const MaterialsDescr = () => (
  <div className="info-block materials-descr">
    <div className="info-block-left-wrapper block-wrapper">
      <div className="materials-descr-img1">
        <img src={`${process.env.PUBLIC_URL}/img/materials-descr-img1.png`} alt="img" />
      </div>

      <div className="info-block-left-wrap block-wrap">
        <div className="container-left">
          <div className="info-block-left">
            <div className="info-block-title title">
              <span>Древесина</span>
              - живой материал
            </div>

            <div className="info-block-text text-main">
              <p>
                Именно древесина лидирует во всех сферах разработки и создания дизайна интерьера.
              </p>
              <p>
                Современные свойства и условия обработки древесины позволяют создавать самые сложные
                и оригинальные напольные, настольные и настенные светильники и люстры. Весь секрет в
                том, что современная обработка дерева создаёт устойчивость к любым воздействиям:
                перепадам температур, влаге, механическим повреждениям.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div className="info-block-right-wrapper block-wrapper">
      <div className="materials-descr-img2">
        <img src={`${process.env.PUBLIC_URL}/img/materials-descr-img2.png`} alt="img" />
      </div>

      <div className="info-block-right-wrap block-wrap">
        <div className="container-right">
          <div className="info-block-right">
            <div className="info-block-title title">
              <span>Лакокрасочные</span>
              материалы:
            </div>

            <div className="info-block-text text-main">
              <p>
                При изготовлении своих изделий SVETLO применяет лишь самые качественные
                лакокрасочные материалы лучших зарубежных производителей. Нанося их на поверхность
                светильников, мы образуем своеобразную защитную плёнку, что сохраняет целостность
                изделия и предотвращает механические повреждения и надолго сохраняет первозданный
                вид натурального дерева.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default MaterialsDescr;
