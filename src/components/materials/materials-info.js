import React from 'react';
import { Link } from 'react-router-dom';
import './materials-info.scss';

const MaterialsInfo = () => (
  <div
    className="materials-info-wrapper block-wrapper"
    style={{ backgroundImage: `url(${process.env.PUBLIC_URL}/img/materials_background_1.jpg)` }}
  >
    <div className="materials-info-wrap block-wrap">
      <div className="container">
        <div className="materials-info">
          <div className="materials-info-content">
            <h1 className="materials-info-title title-big">
              Из чего сделаны наши
              <span> современные дизайнерские </span>
              светильники?
            </h1>

            <div className="materials-info-text text-main">
              <p>
                В производстве своих изделий SVETLO использует только натуральные материалы. Мы
                отдаём предпочтение цельному дереву. На сегодня это самое качественное, экологически
                чистое сырьё, которое отличается долговечностью и красотой фактуры. В создании
                моделей
                <Link to="/services" className="text-link text-bold"> мастера </Link>
                используют дуб, бук, ясень, акацию и другие виды древесины. До сих пор ни один
                искусственный материал не смог повторить того тепла, которое дарит природа. Поэтому
                наши светильники обладают особой положительной энергетикой.
              </p>

              <p>
                Совершенствуется форма изделия с помощью лакокрасочных материалов (воск, масла,
                обогащённые смолами, лаки) передовых европейских производителей, что придаёт
                стойкость и долговечность покрытию. Данная обработка сохраняет древесине
                первоначальную природную красоту.
              </p>
            </div>
          </div>

          <div className="materials-info-img wow fadeInRightBig">
            <img src={`${process.env.PUBLIC_URL}/img/materials-info-img.png`} alt="img" />
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default MaterialsInfo;
