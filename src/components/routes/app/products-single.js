import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import ProductDescr from '../../products-single/product-descr';
import ProductProp from '../../products-single/product-prop';
import {
  loadAllProducts,
  productSelector,
  productsLoadedSelector,
  productsLoadingSelector,
} from '../../../ducks/products';

class ProductsSingle extends Component {
  componentDidMount() {
    const { loading, loaded, loadAll } = this.props;

    window.scrollTo(0, 0);

    if (!loaded && !loading) {
      loadAll();
    }
  }

  render() {
    const { productData } = this.props;

    if (!productData) {
      return null;
    }

    return (
      <Fragment>
        <ProductDescr data={productData} />
        <ProductProp data={productData} />
      </Fragment>
    );
  }
}

const mapStateToProps = (state, { match: { params: { id } } }) => ({
  productData: productSelector(state, id),
  loading: productsLoadingSelector(state),
  loaded: productsLoadedSelector(state),
});

const mapDispatchToProps = {
  loadAll: loadAllProducts,
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductsSingle);
