import React, { Component, Fragment } from 'react';
import ServicesPromo from '../../services/services-promo';
import ServicesInfo from '../../services/services-info';
import ServicesProcess from '../../services/services-process';
import Feedback from '../../general/feedback/feedback';

class Services extends Component {
  componentDidMount() {
    window.scrollTo(0,0);
  }

  render() {
    return (
      <Fragment>
        <ServicesPromo />
        <ServicesInfo />
        <ServicesProcess />
        <Feedback />
      </Fragment>
    );
  }
}

export default Services;
