import React, { Component, Fragment } from 'react';
import MaterialsInfo from '../../materials/materials-info';
import MaterialsDescr from '../../materials/materials-descr';
import MaterialsRequirements from '../../materials/materials-requirements';
import Feedback from '../../general/feedback/feedback';

class Materials extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }

  render() {
    return (
      <Fragment>
        <MaterialsInfo />
        <MaterialsDescr />
        <MaterialsRequirements />
        <Feedback />
      </Fragment>
    );
  }
}

export default Materials;
