import React, { Component, Fragment } from 'react';
import ContactsContent from '../../contacts/contacts';

class Contacts extends Component {
  componentDidMount() {
    window.scrollTo(0,0);
  }

  render() {
    return (
      <Fragment>
        <ContactsContent />
      </Fragment>
    );
  }
}

export default Contacts;
