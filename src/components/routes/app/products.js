import React, { Component, Fragment } from 'react';
import ProductsPromo from '../../products/products-promo';
import ProductsItems from '../../products/products-items';
import ProductsInfo from '../../products/products-info';

class Products extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }

  render() {
    return (
      <Fragment>
        <ProductsPromo />
        <ProductsItems />
        <ProductsInfo />
      </Fragment>
    );
  }
}

export default Products;
