import React, { Component, Fragment } from 'react';
import AboutInfo from '../../about/about-info';
import AboutAdvatages from '../../about/about-advatages';

class About extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }

  render() {
    return (
      <Fragment>
        <AboutInfo />
        <AboutAdvatages />
      </Fragment>
    );
  }
}

export default About;
