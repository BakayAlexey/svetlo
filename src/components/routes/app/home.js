import React, { Component, Fragment } from 'react';
import HomePromo from '../../home/home-promo';
import HomeProducts from '../../home/home-products';
import HomeMaking from '../../home/home-making';
import HomeFeatures from '../../home/home-features';
import Feedback from '../../general/feedback/feedback';
import HomeFaq from '../../home/home-faq';
import HomeReviews from '../../home/home-reviews';

class Home extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }


  render() {
    return (
      <Fragment>
        <HomePromo />
        <HomeProducts />
        <HomeMaking />
        <HomeFeatures />
        <HomeFaq />
        <Feedback />
        <HomeReviews />
      </Fragment>
    );
  }
}

export default Home;
