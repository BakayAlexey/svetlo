import React from 'react';
import './footer.scss';

const Footer = () => (
  <footer className="page-footer">
    <div className="container">
      <div className="page-footer-inner-wrapper">
        <div className="page-footer-block page-footer-phrase title">
          <span>
            Остались
          </span>
          <br />
          вопросы?
        </div>

        <div className="page-footer-block page-footer-copyright text-main">
          © 2018–2019
          <br />
          SVETLO.com.ua
        </div>

        <div className="page-footer-block page-footer-contacts page-footer-phone">
          <div className="page-footer-contacts-inner-wrapper">
            <div className="page-footer-contacts-title title-section">
              <span className="elem-underline">
                П
              </span>
              озвоните нам:
            </div>

            <div className="page-footer-contacts-items">
              <a href="_" className="page-footer-contacts-item text-main">
                +38 (066) 8677079
              </a>
              <a href="_" className="page-footer-contacts-item text-main">
                +38 (063) 8677079
              </a>
            </div>
          </div>
        </div>

        <div className="page-footer-block page-footer-contacts page-footer-mail">
          <div className="page-footer-contacts-inner-wrapper">
            <div className="page-footer-contacts-title title-section">
              <span className="elem-underline">
                Н
              </span>
              апишите нам:
            </div>

            <div className="page-footer-contacts-items">
              <a href="_" className="page-footer-contacts-item text-main">
                ua.svetlo@gmail.com
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
);

export default Footer;
