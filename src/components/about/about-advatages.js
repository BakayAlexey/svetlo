import React from 'react';
import { Link } from 'react-router-dom';
import './about-advatages.scss';

const AboutAdvatages = () => (
  <div
    className="advatages-wrapper block-wrapper"
    style={{ backgroundImage: `url(${process.env.PUBLIC_URL}/img/advatages-bg.jpg)` }}
  >
    <div className="advatages-wrap block-wrap">
      <div className="container">
        <div className="advatages block">

          <div className="advatages-descr">
            <h2 className="advatages-descr-title title">
              Что
              <span> самое главное </span>
              в нашей работе
            </h2>

            <div className="advatages-descr-text text-main">
              <p>
                Наша миссия, а скорее долг - обеспечить наших клиентов не просто качественным и
                натуральным освещением, а еще и красивым. Задумываясь о дизайне собственного
                интерьера, люди не всегда сразу осознают насколько важно освещение. Даже самый
                простой настенный светильник из дерева может подарить особую атмосферу своим
                направленным мягким светом, текстурой, формой и натуральным цветом.
              </p>
              <p>
                Также очень важно учитывать то, что у нас есть четкое правило - мы работаем только с
                экологически чистыми и качественными
                <Link to="/materials" className="text-link text-bold"> материалами</Link>
                .
              </p>
              <p>
                Мы не любим вычурности и помпезности в своих работах - вся красота заключается в
                простоте. Но это не обычная простота, она продумана до каждой мелочи. Только при
                таком условии каждый светильник будет идеален в вашем интерьере и будет выполнять
                положенную на него миссию.
              </p>
            </div>
          </div>

          <div className="advatages-items-wrapper">
            <h2 className="advatages-items-title title">
              Наши
              <span> достижения </span>
              - это:
            </h2>

            <ul className="advatages-items">
              <li className="advatages-item">
                <div className="advatages-item-num elem-underline">125</div>
                <div className="advatages-item-text text-main title-section">
                  Довольных клиента
                </div>
              </li>

              <li className="advatages-item">
                <div className="advatages-item-num elem-underline">34</div>
                <div className="advatages-item-text text-main title-section">
                  уникальных моделей светильников
                </div>
              </li>

              <li className="advatages-item">
                <div className="advatages-item-num elem-underline">3</div>
                <div className="advatages-item-text text-main title-section">
                  года успешной работы
                </div>
              </li>

              <li className="advatages-item">
                <div className="advatages-item-num elem-underline">45</div>
                <div className="advatages-item-text text-main title-section">
                  статей на независимых сервисах
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default AboutAdvatages;
