import React from 'react';
import './about-info.scss';

const AboutInfo = () => (
  <div
    className="about-info-wrapper block-wrapper"
    style={{ backgroundImage: `url(${process.env.PUBLIC_URL}/img/about-info-bg.jpg)` }}
  >
    <div className="about-info-wrap block-wrap">
      <div className="container">
        <div className="about-info">
          <div className="about-info-content">
            <h2 className="about-info-title title-big">
              Кто
              <span> мы?</span>
            </h2>

            <div className="about-info-text text-main">
              <p>
                Мы – «SVETLO». Команда с неординарным подходом в создании уникальных
                ЭКО-светильников. Сочетание стиля,
                современности и красоты – главные критерии
                <span className="text-bold"> воплощения </span>
                наших
                <span className="text-bold"> идей </span>
                в дизайнерском свете. Минимум деталей,
                ровные линии, приятная цветовая гамма создадут комфортную атмосферу любого
                интерьера. Производство
                <span className="text-bold"> качественной продукции </span>
                с учётом индивидуальных пожеланий каждого клиента
                – девиз компании «СВЕТЛО». Мы всегда рады предложить авторские несерийные
                светильники, сделанные
                <span className="text-bold"> из натуральных материалов.</span>
              </p>
            </div>
          </div>

          <div className="about-info-phrase-wrapper">
            <div className="about-info-phrase text-main">
              Ручная работа наших мастеров делает модели «SVETLO» исключительными.
            </div>
          </div>

          <div className="about-info-img">
            <img src={`${process.env.PUBLIC_URL}/img/about-info-img.png`} alt="img" />
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default AboutInfo;
