import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import './header.scss';

class Header extends Component {
  state = {
    isOpen: false,
  };

  static propTypes = {
    navs: PropTypes.array,
  };

  static defaultProps = {
    navs: [],
  };

  // componentDidMount() {
  //   this.body = document.querySelector('body');
  //   this.body.addEventListener('click', this.handleClickOutside);
  // }
  //
  // componentWillUnmount() {
  //   this.body.removeEventListener('click', this.handleClickOutside);
  // }
  //
  // handleClickOutside = (e) => {
  //   const { isOpen } = this.state;
  //   const headerNode = findDOMNode(this);
  //   if ((isOpen && (!headerNode || !headerNode.contains(e.target)))) {
  //     this.state({
  //       isOpen: false,
  //     });
  //   }
  // };

  handleClickToggleNav = () => {
    this.setState(state => ({
      isOpen: !state.isOpen,
    }));
  };

  handleClickLink = () => {
    const { isOpen } = this.state;
    if (isOpen && window.innerWidth < 992) {
      this.setState({
        isOpen: false,
      });
    }
  };

  setToggleNode = (node) => {
    this.toggleNav = node;
  };

  render() {
    const { isOpen } = this.state;
    const { navs } = this.props;

    const navList = navs.map(({ id, to, descr }) => (
      <li key={id}>
        <NavLink to={to} onClick={this.handleClickLink} exact>
          {descr}
        </NavLink>
      </li>
    ));

    return (
      <header className="page-header">
        <div className="container">
          <div className="page-header-inner-wrapper">
            <Link to="/" className="page-header-logo">
              <img src={`${process.env.PUBLIC_URL}/img/svetlo_logo.svg`} alt="logo" />
            </Link>

            <nav className={isOpen ? 'main-nav opened' : 'main-nav'}>
              <div className="main-nav-inner-wrapper">
                <ul className="main-nav-items">
                  {navList}
                </ul>
              </div>
            </nav>

            <div
              className={isOpen ? 'main-nav-toggle opened' : 'main-nav-toggle'}
              ref={this.setToggleNode}
              onClick={this.handleClickToggleNav}
            >
              open menu
            </div>
          </div>
        </div>
      </header>
    );
  }
}

export default Header;
