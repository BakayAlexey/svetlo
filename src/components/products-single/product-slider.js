import React, { Component } from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.scss';
import 'slick-carousel/slick/slick-theme.scss';

class ProductSlider extends Component {
  state = {
    nav1: null,
    nav2: null,
  };

  componentDidMount() {
    this.setState({
      nav1: this.slider1,
      nav2: this.slider2,
    });
  }

  setSlider1Node = (node) => {
    this.slider1 = node;
  };

  setSlider2Node = (node) => {
    this.slider2 = node;
  };

  render() {
    const { images } = this.props;
    const { nav1, nav2 } = this.state;

    const imagesSlides = images.map(image => (
      <img key={image.id} src={image.url} alt="img" />
    ));

    const settingTopSlider = {
      className: 'top-slider',
      asNavFor: nav2,
      infinite: true,
    };

    const settingPagingSlider = {
      className: 'paging-slider',
      asNavFor: nav1,
      infinite: true,
      slidesToShow: 5,
      slidesToScroll: 1,
      swipeToSlide: true,
      focusOnSelect: true,
      variableWidth: true,
    };

    return (
      <div>
        <Slider
          ref={this.setSlider1Node}
          {...settingTopSlider}
        >
          {imagesSlides}
        </Slider>

        <Slider
          ref={this.setSlider2Node}
          {...settingPagingSlider}
        >
          {imagesSlides}
        </Slider>
      </div>
    );
  }
}

export default ProductSlider;
