import React from 'react';
import { Link } from 'react-router-dom';
import ProductSlider from './product-slider';
import './product-descr.scss';

const ProductDescr = (props) => {
  const {
    data: {
      fname,
      price,
      descr,
      images,
    },
  } = props;

  return (
    <div className="product-descr">
      <div className="product-descr-gallery-wrapper block-wrapper">
        <div className="product-descr-gallery-wrap block-wrap">
          <div className="container-left">
            <div className="product-descr-bread-crumbs text-main">
              <Link to="/products" className="text-main">Товары</Link>
              <span> - </span>
              <span className="text-bold">{fname}</span>
            </div>

            <div className="product-descr-gallery">
              <ProductSlider images={images} />
            </div>
          </div>
        </div>
      </div>

      <div className="product-descr-content-wrapper block-wrapper">
        <div className="product-descr-content-wrap block-wrap">
          <div className="container-right">
            <div className="product-descr-content">
              <div className="product-descr-name title-big">
                <span>{fname}</span>
              </div>
              <div className="product-descr-price title">
                <span>
                  {price}
                  грн.
                </span>
              </div>
              <div className="product-descr-text text-main">
                <div className="product-descr-text-title text-bold">
                  Описание:
                </div>
                <p>{descr}</p>
              </div>
              <div className="product-descr-btn-wrapper">
                <button type="button" className="product-descr-btn btn popup-open-btn">Заказать</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductDescr;
