import React from 'react';
import './product-prop.scss';

const ProductProp = (props) => {
  const {
    data: {
      group,
      type,
      typeLighting,
      voltage,
      power,
      material,
      sizes,
    },
  } = props;

  return (
    <div className="product-prop">
      <div
        className="product-prop-img"
        style={{ backgroundImage: `url(${process.env.PUBLIC_URL}/img/product-prop-img.jpg)` }}
      />

      <div className="product-prop-content-wrapper block-wrapper">
        <div className="product-prop-content-wrap block-wrap">
          <div className="container-right">
            <div className="product-prop-content">
              <div className="product-prop-title title">
                <span>Характеристики:</span>
              </div>

              <ul className="product-prop-items text-main">
                {group
                && (
                  <li className="product-prop-item">
                    <div className="product-prop-item-name">Группа товаров</div>
                    <div className="product-prop-item-value">{group}</div>
                  </li>
                )}

                {type
                && (
                  <li className="product-prop-item">
                    <div className="product-prop-item-name">Тип</div>
                    <div className="product-prop-item-value">{type}</div>
                  </li>
                )}

                {typeLighting
                && (
                  <li className="product-prop-item">
                    <div className="product-prop-item-name">Тип элемента освещения</div>
                    <div className="product-prop-item-value">{typeLighting}</div>
                  </li>
                )}

                {voltage
                && (
                  <li className="product-prop-item">
                    <div className="product-prop-item-name">Питающее напряжение</div>
                    <div className="product-prop-item-value">{voltage}</div>
                  </li>
                )}

                {power
                && (
                  <li className="product-prop-item">
                    <div className="product-prop-item-name">Мощность</div>
                    <div className="product-prop-item-value">{power}</div>
                  </li>
                )}

                {material
                && (
                  <li className="product-prop-item">
                    <div className="product-prop-item-name">Материал</div>
                    <div className="product-prop-item-value">{material}</div>
                  </li>
                )}

                {sizes
                && (
                  <li className="product-prop-item">
                    <div className="product-prop-item-name">Размеры (ВхШхГ)</div>
                    <div className="product-prop-item-value">{sizes}</div>
                  </li>
                )}
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductProp;
