import React from 'react';
import { Link } from 'react-router-dom';
import './feedback.scss';

const Feedback = () => (
  <div className="feedback-wrapper">
    <div className="feedback-wrap">
      <div className="container">
        <div className="feedback">
          <div className="feedback-title title">
            Закажите прямо сейчас
            <span> светильник из любой древесины</span>
          </div>

          <div className="feedback-descr text-main">
            Мы гарантируем
            <span className="text-bold"> высокое качество </span>
            используемых материалов!
          </div>

          <div className="feedback-btn-wrapper">
            <Link to="/products" className="feedback-btn btn popup-open-btn">заказать</Link>
          </div>

          <div className="feedback-img">
            <img src={`${process.env.PUBLIC_URL}/img/feedback-img.png`} alt="img" />
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default Feedback;
