import styled from 'styled-components';
import {
  largeWidths,
  mediumWidths,
  smallWidths,
  extraSmallWidths,
} from '../../constants/media-width';

export const Container = styled.div`
  box-sizing: border-box;
  width: 100%;
  max-width: 1360px;
  margin: 0 auto;
  padding: 0 20px;
  
  @media ${largeWidths} {
    max-width: 1180px;
  }
  
  @media ${mediumWidths} {
    max-width: 980px;
  }
  
  @media ${smallWidths} {
    width: 760px;
  }
  
  @media ${extraSmallWidths} {
    width: 100%;
  }
`;

export const ContainerLeft = styled.div`
  box-sizing: border-box;
  width: 680px;
  padding-left: 20px;
  margin-left: auto;
  
  @media ${largeWidths} (
    width: 590px;
  )
  
  @media ${mediumWidths} (
    width: 490px;
  )
  
  @media ${smallWidths} (
    width: 380px;
  )
  
  @media ${extraSmallWidths} {
    width: 100%;
  }
`;

export const ContainerRight = styled.div`
  box-sizing: border-box;
  width: 680px;
  padding-right: 20px;
  margin-right: auto;
  
  @media ${largeWidths} (
    width: 590px;
  )
  
  @media ${mediumWidths} (
    width: 490px;
  )
  
  @media ${smallWidths} (
    width: 380px;
  )
  
  @media ${extraSmallWidths} {
    width: 100%;
  }
`;
