import React from 'react';
import ProductItem from './product-item';

const ProductsList = (props) => {
  const { productsData } = props;

  if (!productsData || productsData.length === 0) {
    return null;
  }

  const productsList = productsData.map(product => (
    <ProductItem key={product.id} data={product} />
  ));

  return (
    <div className="products-items-wrapper">
      <div className="products-items">
        {productsList}
      </div>
    </div>
  );
};

ProductsList.defaultProps = {
  productData: [],
};

export default ProductsList;
