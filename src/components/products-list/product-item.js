import React from 'react';
import { Link } from 'react-router-dom';

const ProductItem = (props) => {
  const { data } = props;
  return (
    <Link to={`/products/${data.id}`} className="products-item">
      <div className="products-item-img">
        <img src={data.images[0].url} alt="img" />
      </div>
      <div className="products-item-descr">
        <div className="products-item-descr-price">
          {data.price}
          грн.
        </div>
        <div className="products-item-descr-name text-main">
          {data.fname}
        </div>
        <div className="products-item-descr-materials text-main">
          {data.material}
        </div>
      </div>
      <div className="products-item-btn-wrapper">
        <div className="products-item-btn btn-product">
          подробнее
        </div>
      </div>
    </Link>
  );
};

export default ProductItem;
