import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  editImageProduct,
  deleteImageProduct,
} from '../../../ducks/products';

class ProductImg extends Component {
  state = {
    order: '',
  };

  componentDidMount() {
    const { data: { order } } = this.props;
    this.setState({
      order,
    });
  }

  changeOrderSubmitHandler = (e) => {
    e.preventDefault();
    const { editImg, data: { id }, productId } = this.props;
    const { order } = this.state;
    if (order) {
      editImg(productId, id, order);
    }
  };

  changeOrderHandler = (e) => {
    const { value } = e.target;
    const valueCheck = (value && !value.match(/^[\d]+$/)) ? value.replace(/\D/g, '') : value;
    this.setState({
      order: valueCheck,
    });
  };

  deleteImgHandler = () => {
    const { data: { id }, productId, deleteImg } = this.props;
    deleteImg(productId, id);
  };

  render() {
    const { data: { url } } = this.props;
    const { order } = this.state;

    return (
      <li className="admin-img-item">
        <div className="admin-img-item-image">
          <img src={url} alt="img" />
        </div>
        <div className="admin-img-item-info">
          <form onSubmit={this.changeOrderSubmitHandler}>
            <label className="admin-form-block">
              <span>Order: </span>
              <input type="text" name="order" onChange={this.changeOrderHandler} value={order} />
            </label>
            <div className="admin-img-item-btns">
              <button type="submit">Change order</button>
              <button type="button" onClick={this.deleteImgHandler}>Delete</button>
            </div>
          </form>
        </div>
      </li>
    );
  }
}

const mapDispatchToProps = {
  editImg: editImageProduct,
  deleteImg: deleteImageProduct,
};

export default connect(null, mapDispatchToProps)(ProductImg);
