import React, { Component } from 'react';
import { connect } from 'react-redux';
import ProductImgList from './product-img-list';
import {
  editProductItem,
  productImagesSelector,
  productSelector,
  uploadImageProduct,
} from '../../../ducks/products';

class ProductEdit extends Component {
  state = {
    order: '',
    fname: '',
    price: '',
    descr: '',
    material: '',
    group: '',
    type: '',
    typeLighting: '',
    voltage: '',
    power: '',
    sizes: '',
  };

  componentDidMount() {
    const {
      item:
        {
          order,
          fname,
          price,
          descr,
          material,
          group,
          type,
          typeLighting,
          voltage,
          power,
          sizes,
        },
    } = this.props;
    this.setState({
      order,
      fname,
      price,
      descr,
      material,
      group,
      type,
      typeLighting,
      voltage,
      power,
      sizes,
    });
  }

  changeHandler = (e) => {
    const { name, value } = e.target;

    if (name === 'order') {
      if (value.trim() === '') {
        e.target.className = 'error';
      } else {
        e.target.className = '';
      }
      const valueCheck = (value && !value.match(/^[\d]+$/)) ? value.replace(/\D/g, '') : value;
      this.setState({
        order: valueCheck,
      });
      this.setState({
        order: valueCheck,
      });
    } else if (name === 'fname') {
      if (value.trim() === '') {
        e.target.className = 'error';
      } else {
        e.target.className = '';
      }
      this.setState({
        fname: value,
      });
    } else {
      this.setState({
        [name]: value,
      });
    }
  };

  changeImageHandler = (e) => {
    const { match: { params: { id } }, uploadImg } = this.props;
    uploadImg(id, e.target.files[0]);
    e.target.value = '';
  };

  submitHandler = (e) => {
    e.preventDefault();
    const { match: { params: { id } } } = this.props;
    const {
      order,
      fname,
      price,
      descr,
      material,
      group,
      type,
      typeLighting,
      voltage,
      power,
      sizes,
    } = this.state;
    const { editItem } = this.props;
    if (order.trim() !== '' && fname.trim() !== '') {
      editItem({
        id,
        order,
        fname,
        price,
        descr,
        material,
        group,
        type,
        typeLighting,
        voltage,
        power,
        sizes,
      });
    }
  };

  render() {
    const {
      order,
      fname,
      price,
      descr,
      material,
      group,
      type,
      typeLighting,
      voltage,
      power,
      sizes,
    } = this.state;
    const { images, match: { params: { id } } } = this.props;

    return (
      <div className="admin-add">
        <div className="container">
          <form className="admin-form" onSubmit={this.submitHandler}>
            <label className="admin-form-block">
              <span>Order</span>
              <input type="text" name="order" onChange={this.changeHandler} value={order} />
            </label>
            <label className="admin-form-block">
              <span>Название</span>
              <input type="text" name="fname" onChange={this.changeHandler} value={fname} />
            </label>
            <label className="admin-form-block">
              <span>Цена</span>
              <input type="num" name="price" onChange={this.changeHandler} value={price} />
            </label>
            <label className="admin-form-block">
              <span>Описание</span>
              <textarea name="descr" onChange={this.changeHandler} value={descr} />
            </label>
            <label className="admin-form-block">
              <span>Материал</span>
              <input type="text" name="material" onChange={this.changeHandler} value={material} />
            </label>
            <label className="admin-form-block">
              <span>Группа товаров</span>
              <input type="text" name="group" onChange={this.changeHandler} value={group} />
            </label>
            <label className="admin-form-block">
              <span>Вид</span>
              <input type="text" name="type" onChange={this.changeHandler} value={type} />
            </label>
            <label className="admin-form-block">
              <span>Тип элемента освещения</span>
              <input type="text" name="typeLighting" onChange={this.changeHandler} value={typeLighting} />
            </label>
            <label className="admin-form-block">
              <span>Питающее напряжение</span>
              <input type="text" name="voltage" onChange={this.changeHandler} value={voltage} />
            </label>
            <label className="admin-form-block">
              <span>Мощность</span>
              <input type="text" name="power" onChange={this.changeHandler} value={power} />
            </label>
            <label className="admin-form-block">
              <span>Размеры (ВхШхГ)</span>
              <input type="text" name="sizes" onChange={this.changeHandler} value={sizes} />
            </label>
            <label className="admin-form-block">
              <span>Загрузить фото</span>
              <input type="file" name="add-image" onChange={this.changeImageHandler} />
            </label>

            <div className="admin-form-btn-wrapper">
              <input type="submit" className="admin-form-btn" value="Редактировать" />
            </div>
          </form>

          {(images && images.length > 0) ? <ProductImgList id={id} dataImgs={images} /> : null }
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, { match: { params: { id } } }) => ({
  item: productSelector(state, id),
  images: productImagesSelector(state, id),
});

const mapDispatchToProps = {
  editItem: editProductItem,
  uploadImg: uploadImageProduct,
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductEdit);
