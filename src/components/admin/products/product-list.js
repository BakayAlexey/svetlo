import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import {
  productsItemsSelector,
  productsLoadedSelector,
  productsLoadingSelector,
  loadAllProducts,
  deleteProductItem,
} from '../../../ducks/products';

class ProductList extends Component {
  componentDidMount() {
    const { loading, loaded, loadAll } = this.props;
    if (!loading && !loaded) {
      loadAll();
    }
  }

  btnDeleteHandler = (id) => {
    const { deleteItem } = this.props;
    deleteItem(id);
  };

  getList = () => {
    const { items, match: { path } } = this.props;

    if (items.length === 0) {
      return null;
    }

    const itemsSorted = items.sort((a, b) => {
      if (+a.order > +b.order) return 1;
      if (+a.order < +b.order) return -1;
      return 0;
    });

    return (
      itemsSorted
        .map(({
          id,
          order,
          fname,
          images,
        }) => (
          <li key={id} className="admin-item">
            {(images && images.length > 0) ? (
              <div className="admin-item-img">
                <img src={images[0].url} alt="img" />
              </div>) : null}

            <div className="admin-item-val">
              <div>Order:</div>
              <div>{order}</div>
            </div>
            <div className="admin-item-val">
              <div>Name:</div>
              <div>{fname}</div>
            </div>
            <div className="admin-item-btns">
              <Link
                className="admin-item-btn"
                to={`${path}/${id}`}
              >
                Edit
              </Link>
              <button
                type="button"
                className="admin-item-btn"
                onClick={() => this.btnDeleteHandler(id)}
              >
                Delete
              </button>
            </div>
          </li>
        ))
    );
  };

  render() {
    const { match: { path } } = this.props;

    return (
      <div className="admin-list-wrapper">
        <div className="container">
          <Link
            to={`${path}/add`}
            className="admin-btn-add"
          >
            Add
          </Link>
          <ul className="admin-list">
            {this.getList()}
          </ul>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  items: productsItemsSelector(state),
  loading: productsLoadingSelector(state),
  loaded: productsLoadedSelector(state),
});

const mapDispatchToProps = {
  loadAll: loadAllProducts,
  deleteItem: deleteProductItem,
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ProductList));
