import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addProductItem } from '../../../ducks/products';

class ProductAdd extends Component {
  state = {
    order: '',
    fname: '',
    price: '',
    descr: '',
    material: '',
    group: '',
    type: '',
    typeLighting: '',
    voltage: '',
    power: '',
    sizes: '',
  };

  changeHandler = (e) => {
    const { value, name } = e.target;

    if (name === 'order') {
      if (value.trim() === '') {
        e.target.className = 'error';
      } else {
        e.target.className = '';
      }
      this.setState({
        order: value,
      });
    } else {
      this.setState({
        [name]: value,
      });
    }
  };

  submitHandler = (e) => {
    e.preventDefault();
    const {
      order,
      fname,
      price,
      descr,
      material,
      group,
      type,
      typeLighting,
      voltage,
      power,
      sizes,
    } = this.state;
    const { addItem } = this.props;
    if (order.trim() !== '' && fname.trim() !== '') {
      addItem({
        order,
        fname,
        price,
        descr,
        material,
        group,
        type,
        typeLighting,
        voltage,
        power,
        sizes,
      });
    }
  };

  render() {
    const {
      order,
      fname,
      price,
      descr,
      material,
      group,
      type,
      typeLighting,
      voltage,
      power,
      sizes,
    } = this.state;

    return (
      <div className="admin-faq-add">
        <div className="container">
          <form className="admin-form" onSubmit={this.submitHandler}>
            <label className="admin-form-block">
              <span>Order</span>
              <input type="text" name="order" onChange={this.changeHandler} value={order} />
            </label>
            <label className="admin-form-block">
              <span>Название</span>
              <input type="text" name="fname" onChange={this.changeHandler} value={fname} />
            </label>
            <label className="admin-form-block">
              <span>Цена</span>
              <input type="num" name="price" onChange={this.changeHandler} value={price} />
            </label>
            <label className="admin-form-block">
              <span>Описание</span>
              <textarea name="descr" onChange={this.changeHandler} value={descr} />
            </label>
            <label className="admin-form-block">
              <span>Материал</span>
              <input type="text" name="material" onChange={this.changeHandler} value={material} />
            </label>
            <label className="admin-form-block">
              <span>Группа товаров</span>
              <input type="text" name="group" onChange={this.changeHandler} value={group} />
            </label>
            <label className="admin-form-block">
              <span>Вид</span>
              <input type="text" name="type" onChange={this.changeHandler} value={type} />
            </label>
            <label className="admin-form-block">
              <span>Тип элемента освещения</span>
              <input type="text" name="typeLighting" onChange={this.changeHandler} value={typeLighting} />
            </label>
            <label className="admin-form-block">
              <span>Питающее напряжение</span>
              <input type="text" name="voltage" onChange={this.changeHandler} value={voltage} />
            </label>
            <label className="admin-form-block">
              <span>Мощность</span>
              <input type="text" name="power" onChange={this.changeHandler} value={power} />
            </label>
            <label className="admin-form-block">
              <span>Размеры (ВхШхГ)</span>
              <input type="text" name="sizes" onChange={this.changeHandler} value={sizes} />
            </label>

            <div className="admin-form-btn-wrapper">
              <input type="submit" className="admin-form-btn" value="Добавить" />
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default connect(null, { addItem: addProductItem })(ProductAdd);
