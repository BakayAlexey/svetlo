import React from 'react';
import ProductImg from './product-img';

const ProductImgList = (props) => {
  const { id, dataImgs } = props;
  return (
    <ul className="admin-img-list">
      {dataImgs.map(data => (
        <ProductImg key={data.id} data={data} productId={id} />
      ))}
    </ul>
  );
};

export default ProductImgList;
