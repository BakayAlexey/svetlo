import React, { Component } from 'react';
import { connect } from 'react-redux';
import { signIn } from '../../../ducks/auth';
import './sign-in.scss';

class SignIn extends Component {
  state ={
    email: '',
    password: '',
  };

  changeEmailHandler = (e) => {
    this.setState({
      email: e.target.value,
    });
  };

  changePasswordHandler = (e) => {
    this.setState({
      password: e.target.value,
    });
  };

  submitHandler = (e) => {
    const { signInAction } = this.props;
    const { email, password } = this.state;

    e.preventDefault();

    signInAction(email, password);
  };

  render() {
    const { email, password } = this.state;

    return (
      <div className="sign-in-wrapper">
        <div className="container">
          <form className="sign-in" onSubmit={this.submitHandler}>
            <label className="form-block">
              <span>E-mail</span>
              <input type="email" name="email" onChange={this.changeEmailHandler} value={email} />
            </label>
            <label className="form-block">
              <span>Пароль</span>
              <input type="password" name="password" onChange={this.changePasswordHandler} value={password} />
            </label>

            <div className="form-btn-wrapper">
              <input type="submit" className="form-btn" value="Войти" />
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default connect(null, { signInAction: signIn })(SignIn);
