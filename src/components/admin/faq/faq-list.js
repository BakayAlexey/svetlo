import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import {
  faqItemsSelector,
  faqLoadedSelector,
  faqLoadingSelector,
  loadAllFaq,
  deleteFaqItem,
} from '../../../ducks/faq';

class FaqList extends Component {
  componentDidMount() {
    const { loading, loaded, loadAllAction } = this.props;
    if (!loading && !loaded) {
      loadAllAction();
    }
  }

  btnDeleteHandler = (id) => {
    const { deleteItemAction } = this.props;
    deleteItemAction(id);
  };

  getItems = () => {
    const { items, match: { path } } = this.props;

    if (items.length === 0) {
      return null;
    }

    const itemsSorted = items.sort((a, b) => {
      if (+a.order > +b.order) return 1;
      if (+a.order < +b.order) return -1;
      return 0;
    });

    return (
      itemsSorted.map(({
        id,
        order,
        question,
        answer,
      }) => (
        <li key={id} className="admin-item">
          <div className="admin-item-val">
            <div>id:</div>
            <div>{id}</div>
          </div>
          <div className="admin-item-val">
            <div>Order:</div>
            <div>{order}</div>
          </div>
          <div className="admin-item-val">
            <div>Question:</div>
            <div>{question}</div>
          </div>
          <div className="admin-item-val">
            <div>Answer:</div>
            <div>{answer}</div>
          </div>
          <div className="admin-item-btns">
            <Link
              className="admin-item-btn"
              to={`${path}/${id}`}
            >
              Edit
            </Link>
            <button
              type="button"
              className="admin-item-btn"
              onClick={() => this.btnDeleteHandler(id)}
            >
              Delete
            </button>
          </div>
        </li>
      ))
    );
  };

  render() {
    const { match: { path } } = this.props;

    return (
      <div className="admin-list-wrapper">
        <div className="container">
          <Link
            to={`${path}/add`}
            className="admin-btn-add"
          >
            Add
          </Link>
          <ul className="admin-list">
            {this.getItems()}
          </ul>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  items: faqItemsSelector(state),
  loading: faqLoadingSelector(state),
  loaded: faqLoadedSelector(state),
});

const mapDispatchToProps = {
  loadAllAction: loadAllFaq,
  deleteItemAction: deleteFaqItem,
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(FaqList));
