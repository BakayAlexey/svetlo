import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addFaqItem } from '../../../ducks/faq';

class FaqAdd extends Component {
  state = {
    order: '',
    question: '',
    answer: '',
  };

  changeOrderHandler = (e) => {
    const { value } = e.target;
    if (value.trim() === '') {
      e.target.className = 'error';
    } else {
      e.target.className = '';
    }
    this.setState({
      order: value,
    });
  };

  changeQuestionHandler = (e) => {
    this.setState({
      question: e.target.value,
    });
  };

  changeAnswerHandler = (e) => {
    const { value } = e.target;
    if (value.trim() === '') {
      e.target.className = 'error';
    } else {
      e.target.className = '';
    }
    this.setState({
      answer: value,
    });
  };

  submitHandler = (e) => {
    e.preventDefault();
    const { order, question, answer } = this.state;
    const { addFaqItemAction } = this.props;
    if (answer.trim() !== '' && question.trim() !== '') {
      addFaqItemAction(order, question, answer);
    }
  };

  render() {
    const { order, question, answer } = this.state;

    return (
      <div className="admin-faq-add">
        <div className="container">
          <form className="admin-form" onSubmit={this.submitHandler}>
            <label className="admin-form-block">
              <span>Order</span>
              <input type="text" name="order" onChange={this.changeOrderHandler} value={order} />
            </label>
            <label className="admin-form-block">
              <span>Вопрос</span>
              <textarea name="question" onChange={this.changeQuestionHandler} value={question} />
            </label>
            <label className="admin-form-block">
              <span>Ответ</span>
              <textarea name="ask" onChange={this.changeAnswerHandler} value={answer} />
            </label>

            <div className="admin-form-btn-wrapper">
              <input type="submit" className="admin-form-btn" value="Добавить" />
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default connect(null, { addFaqItemAction: addFaqItem })(FaqAdd);
