import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  editFaqItem,
  faqSelector,
} from '../../../ducks/faq';

class FaqEdit extends Component {
  state = {
    order: '',
    question: '',
    answer: '',
  };

  componentDidMount() {
    const { faqItem: { order, question, answer } } = this.props;
    this.setState({
      order, question, answer,
    });
  }

  changeOrderHandler = (e) => {
    const { value } = e.target;
    if (value.trim() === '') {
      e.target.className = 'error';
    } else {
      e.target.className = '';
    }
    this.setState({
      order: value,
    });
  };

  changeQuestionHandler = (e) => {
    this.setState({
      question: e.target.value,
    });
  };

  changeAnswerHandler = (e) => {
    const { value } = e.target;
    if (value.trim() === '') {
      e.target.className = 'error';
    } else {
      e.target.className = '';
    }
    this.setState({
      answer: value,
    });
  };

  submitHandler = (e) => {
    e.preventDefault();
    const { match: { params: { id } } } = this.props;
    const { order, question, answer } = this.state;
    const { editItem } = this.props;
    if (answer.trim() !== '' && question.trim() !== '') {
      editItem(id, order, question, answer);
    }
  };

  render() {
    const { faqItem } = this.props;
    const { order, question, answer } = this.state;

    if (!faqItem) {
      return (
        <div>No data!!!</div>
      );
    }

    return (
      <div className="admin-faq-add">
        <div className="container">
          <form className="admin-form" onSubmit={this.submitHandler}>
            <label className="admin-form-block">
              <span>Order</span>
              <input type="text" name="order" onChange={this.changeOrderHandler} value={order} />
            </label>
            <label className="admin-form-block">
              <span>Ответ</span>
              <textarea name="question" onChange={this.changeQuestionHandler} value={question} />
            </label>
            <label className="admin-form-block">
              <span>Вопрос</span>
              <textarea name="ask" onChange={this.changeAnswerHandler} value={answer} />
            </label>

            <div className="admin-form-btn-wrapper">
              <input type="submit" className="admin-form-btn" value="Редактировать" />
            </div>
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, { match: { params: { id } } }) => ({
  faqItem: faqSelector(state, id),
});

const mapDispatchToProps = {
  editItem: editFaqItem,
};

export default connect(mapStateToProps, mapDispatchToProps)(FaqEdit);
