import React, { Component } from 'react';
import Map from './map';
import './contacts.scss';

class Contacts extends Component {
  state = {
    showMap: false,
  };

  toggleMap = () => {
    this.setState(state => ({
      showMap: !state.showMap,
    }));
  };

  render() {
    const { showMap } = this.state;

    return (
      <div className="contacts-block-wrapper">
        <div className="contacts-block">

          <div className="contacts-wrapper block-wrapper">
            <div className="contacts-wrap block-wrap">
              <div className="container-left">
                <div className="contacts">
                  <div className="contacts-title title-big">
                    Наши
                    <span> контакты</span>
                  </div>

                  <div className="contacts-items">
                    <div className="contacts-item">
                      <div className="contacts-item-title elem-underline">
                        Tel
                      </div>

                      <div className="contacts-item-content">
                        <span className="text-main">+38 (066) 867 70 79</span>
                        <br />
                        <span className="text-main">+38 (063) 867 70 79</span>
                      </div>
                    </div>

                    <div className="contacts-item">
                      <div className="contacts-item-title elem-underline">
                        E-mail
                      </div>

                      <div className="contacts-item-content">
                        <span className="text-main">ua.svetlo@gmail.com</span>
                        <br />
                      </div>
                    </div>

                    <div className="contacts-item">
                      <div className="contacts-item-title elem-underline">
                        Address
                      </div>

                      <div className="contacts-item-content text-main">
                        Украина, Киев, ул. Максима Берлинского 19,
                        <br />
                        офис 7
                      </div>
                    </div>
                  </div>

                  <div className="contacts-btn-wrapper">
                    <button
                      type="button"
                      className="contacts-btn btn"
                      onClick={this.toggleMap}
                    >
                      {showMap ? 'Назад' : 'На карте'}
                    </button>
                  </div>
                </div>
              </div>
            </div>

            <div className={showMap ? 'map-wrapper opened' : 'map-wrapper'}>
              <Map />
            </div>
          </div>

          <div
            className="request-wrapper block-wrapper"
            style={{ backgroundImage: `url("${process.env.PUBLIC_URL}/img/contacts-bg.jpg")` }}
          >
            <div className="request-wrap block-wrap">
              <div className="container-right">
                <div className="request">
                  <div className="request-title title">
                    <span>Заполните форму </span>
                    и мы свяжемся с Вами
                    <span> в&#160;ближайшее время!</span>
                  </div>

                  <form className="request-form form" action="#" method="post">
                    <div className="form-input-text">
                      <input type="text" name="fname" placeholder="имя" required />
                    </div>

                    <div className="form-input-text">
                      <input type="email" name="email" placeholder="e-mail" required />
                    </div>

                    <div className="form-textarea">
                      <textarea name="message" placeholder="сообщение" required />
                    </div>

                    <div className="form-input-btn">
                      <input type="submit" name="submit" className="btn" value="отправить" />
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    );
  }
}

export default Contacts;
