import React from 'react';
import { compose, withProps, withStateHandlers } from 'recompose';
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
  InfoWindow,
} from 'react-google-maps';

const MapContent = compose(
  withStateHandlers(() => ({
    isOpen: false,
  }), {
    onToggleOpen: ({ isOpen }) => () => ({
      isOpen: !isOpen,
    }),
  }),
  withProps({
    googleMapURL: 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBUML2ts5SPJoWHmNvPRUF2swbjxsivnFU',
    loadingElement: <div style={{ width: '100%', height: '100%' }} />,
    containerElement: <div style={{ width: '100%', height: '100%' }} />,
    mapElement: <div style={{ width: '100%', height: '100%' }} />,
  }),
  withScriptjs,
  withGoogleMap,
)(props => (
  <GoogleMap defaultZoom={16} defaultCenter={{ lat: 50.47581, lng: 30.44097 }}>
    <Marker
      position={{ lat: 50.47581, lng: 30.44097 }}
      onClick={props.onToggleOpen}
    >
      {
        props.isOpen
        && (
          <InfoWindow onCloseClick={props.onToggleOpen}>
            <p>InfoWindow</p>
          </InfoWindow>
        )
      }
    </Marker>
  </GoogleMap>
));

export default MapContent;
