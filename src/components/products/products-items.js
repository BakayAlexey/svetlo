import React, { Component } from 'react';
import { connect } from 'react-redux';
import ProductList from '../products-list/products-list';
import {
  loadAllProducts,
  productsItemsSelector,
  productsLoadedSelector,
  productsLoadingSelector,
} from '../../ducks/products';

class ProductsItems extends Component {
  componentDidMount() {
    const { loading, loaded, loadAll } = this.props;
    if (!loaded && !loading) {
      loadAll();
    }
  }

  render() {
    const { productsData } = this.props;

    if (!productsData || productsData.length === 0) {
      return null;
    }

    const productsDataSorted = productsData.sort((a, b) => {
      if (+a.order > +b.order) return 1;
      if (+a.order < +b.order) return -1;
      return 0;
    });

    return (
      <div className="products-wrapper products-main block-wrapper">
        <div className="products-wrap block-wrap">
          <div className="container">
            <div className="products block">
              <h2 className="products-title title">
                Наши
                <span> товары:</span>
              </h2>

              <ProductList productsData={productsDataSorted} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  productsData: productsItemsSelector(state),
  loading: productsLoadingSelector(state),
  loaded: productsLoadedSelector(state),
});

const mapDispatchToProps = {
  loadAll: loadAllProducts,
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductsItems);
