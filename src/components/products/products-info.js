import React from 'react';
import './products-info.scss';

const ProductsInfo = () => {
  return (
    <div className="info-block products-info-block">
      <div className="info-block-left-wrapper block-wrapper">
        <div className="products-info-block-img1 wow fadeInLeftBig">
          <img src={`${process.env.PUBLIC_URL}/img/products-info-img1.png`} alt="img" />
        </div>

        <div className="info-block-left-wrap block-wrap">
          <div className="container-left">
            <div className="info-block-left">
              <div className="info-block-title title">
                <span>Деревянный светильник </span>
                - источник вдохновения для интерьера
              </div>

              <div className="info-block-text text-main">
                <p>
                  <span className="text-bold">Дизайнерский светильник </span>
                  подойдет к любому типу интерьера. К классическому интерьеру - светильники из
                  ценных пород древесины, с украшениями и тонкой обработкой росписи. Минимализм или
                  хай-тек примет светильники с естественным рисунком дерева, прямыми и строгими
                  формами и линиями, отсутствием украшений. Прованс, кантри - добавляем цветочные
                  детали, плетение и ковку. Арт-деко и гламур - добавляем яркости причудливыми
                  завитками, богатым декором или множеством деталей.
                </p>
                <p>
                  Дизайн в современном мире любит играть с формами, цветами и текстурами. Именно
                  поэтому идеальное помещение для древесины - практичное пространство с естественной
                  палитрой и комфортом. Дизайнерские
                  <span className="text-bold"> люстры из дерева </span>
                  и светильники являются залогом уюта, чувства натуральности окружения, тонкого
                  чувства стиля и даже особого запаха древесины.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="info-block-right-wrapper block-wrapper">
        <div className="products-info-block-img2 wow fadeInRightBig">
          <img src={`${process.env.PUBLIC_URL}/img/products-info-img2.png`} alt="img" />
        </div>

        <div className="info-block-right-wrap block-wrap">
          <div className="container-right">
            <div className="info-block-right">
              <div className="info-block-title title">
                <span>Настольный, встроенный, напольный, настенный </span>
                светильник
              </div>

              <div className="info-block-text text-main">
                <p>
                  Практичность древесины в сочетании с современными технологиями обработки и
                  освещения ограничиваются только фантазией мастера. Поэтому светильники могут быть
                  представлены в любых конструкциях и типах освещения. Чем лучше видно натуральность
                  изделия, тем больше нужной атмосферы получает помещение.
                </p>
                <p>
                  Разнятся и типы светильников.
                  <span className="text-bold"> Настенные светильники </span>
                  сами по себе выступают украшениями стен и освещают выбранные элементы точечно.
                  <span className="text-bold"> Встроенные светильники </span>
                  в элементы дизайна позволяют придать световые акценты и игру света другим
                  предметам.
                  <span className="text-bold"> Напольные светильники </span>
                  заполнят пустующие углы и пространство, и впишутся в общий дизайн. А
                  <span className="text-bold"> настольные светильники </span>
                  станут украшением любой тумбы и стола, особенно если они изготовлены с красивой
                  резьбой или оригинальным дизайном. А
                  <span className="text-bold"> люстра из древесины </span>
                  станет звездой помещения и даст максимально много света любому помещению.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductsInfo;
