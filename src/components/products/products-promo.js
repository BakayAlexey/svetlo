import React from 'react';
import './products-promo.scss';

const ProductsPromo = () => (
  <div
    className="products-promo-wrapper block-wrapper"
    style={{ backgroundImage: `url(${process.env.PUBLIC_URL}/img/products-promo-img-bg.jpg)` }}
  >
    <div className="products-promo-wrap block-wrap">
      <div className="container">
        <div className="products-promo">
          <div className="products-promo-content">
            <h2 className="products-promo-title title-big">
              Дизайнерские
              <span> люстры и светильники </span>
              из дерева
            </h2>

            <div className="products-promo-text text-main">
              <p>
                <span className="text-bold">Деревянный светильник </span>
                - это не только экологичность и уют, но и очень мощный источник эстетики оформления
                интерьера. Главная особенность использования любого освещения из дерева - это его
                правильное сочетание с другими предметами в помещении.
              </p>
            </div>
          </div>

          <div className="products-promo-phrase-wrapper">
            <div className="products-promo-phrase text-main">
              <p>
                Несмотря на то, что в прошлом был бум на футуризм и индустриальность, за последние
                несколько лет становится все больше и больше приверженцев натуральных материалов в
                дизайне и интерьере. И именно древесина лидирует во всех сферах разработки и
                создания дизайна интерьера.
              </p>
            </div>

            <div className="products-promo-img wow fadeInRightBig">
              <img src={`${process.env.PUBLIC_URL}/img/products-promo-img.png`} alt="img" />
            </div>
          </div>

          <div className="products-promo-btn-wrapper">
            <a href="_" className="products-promo-btn btn">
              смотреть товары
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default ProductsPromo;
