import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/storage';
import { Record, Map } from 'immutable';
import { createSelector } from 'reselect';
import { push } from 'connected-react-router';
import { appName } from '../config';
import { fbToEntities } from './utils';


// Constants

export const moduleName = 'products';
const prefix = `${appName}/${moduleName}`;

const LOAD_ALL_START = `${prefix}/LOAD_ALL_START`;
const LOAD_ALL_SUCCESS = `${prefix}/LOAD_ALL_SUCCESS`;
const LOAD_ALL_ERROR = `${prefix}/LOAD_ALL_ERROR`;

const ADD_ITEM_REQUEST = `${prefix}/ADD_ITEM_START`;
const ADD_ITEM_SUCCESS = `${prefix}/ADD_ITEM_SUCCESS`;
const ADD_ITEM_ERROR = `${prefix}/ADD_ITEM_ERROR`;

const EDIT_ITEM_REQUEST = `${prefix}/EDIT_ITEM_START`;
const EDIT_ITEM_SUCCESS = `${prefix}/EDIT_ITEM_SUCCESS`;
const EDIT_ITEM_ERROR = `${prefix}/EDIT_ITEM_ERROR`;

const DELETE_ITEM_REQUEST = `${prefix}/DELETE_ITEM_START`;
const DELETE_ITEM_SUCCESS = `${prefix}/DELETE_ITEM_SUCCESS`;
const DELETE_ITEM_ERROR = `${prefix}/DELETE_ITEM_ERROR`;

const UPLOAD_ITEM_IMG_REQUEST = `${prefix}/UPLOAD_ITEM_IMG_REQUEST`;
const UPLOAD_ITEM_IMG_SUCCESS = `${prefix}/UPLOAD_ITEM_IMG_SUCCESS`;
const UPLOAD_ITEM_IMG_ERROR = `${prefix}/UPLOAD_ITEM_IMG_ERROR`;

const EDIT_ITEM_IMG_REQUEST = `${prefix}/EDIT_ITEM_IMG_REQUEST`;
const EDIT_ITEM_IMG_SUCCESS = `${prefix}/EDIT_ITEM_IMG_SUCCESS`;
const EDIT_ITEM_IMG_ERROR = `${prefix}/EDIT_ITEM_IMG_ERROR`;

const DELETE_ITEM_IMG_REQUEST = `${prefix}/DELETE_ITEM_IMG_REQUEST`;
const DELETE_ITEM_IMG_SUCCESS = `${prefix}/DELETE_ITEM_IMG_SUCCESS`;
const DELETE_ITEM_IMG_ERROR = `${prefix}/DELETE_ITEM_IMG_ERROR`;


// Reducer

const ProductRecord = Record({
  id: null,
  order: null,
  fname: null,
  price: null,
  descr: null,
  material: null,
  // specification
  group: null,
  type: null,
  typeLighting: null,
  voltage: null,
  power: null,
  sizes: null,
  // images
  images: [],
});

const ReducerState = Record({
  entities: new Map({}),
  loading: false,
  loaded: false,
  error: null,
});

export default function reducer(state = new ReducerState(), action) {
  const { type, payload, error } = action;

  switch (type) {
    case LOAD_ALL_START:
      return state.set('loading', true);

    case LOAD_ALL_SUCCESS:
      return state
        .set('loading', false)
        .set('loaded', true)
        .set('entities', fbToEntities(payload.productsItems, ProductRecord));

    case LOAD_ALL_ERROR:
      return state.set('error', error);

    case ADD_ITEM_ERROR:
      return state.set('error', error);

    case EDIT_ITEM_SUCCESS:
      return state
        .setIn(['entities', payload.id, 'order'], payload.order)
        .setIn(['entities', payload.id, 'fname'], payload.fname)
        .setIn(['entities', payload.id, 'price'], payload.price)
        .setIn(['entities', payload.id, 'descr'], payload.descr)
        .setIn(['entities', payload.id, 'material'], payload.material)
        .setIn(['entities', payload.id, 'group'], payload.group)
        .setIn(['entities', payload.id, 'type'], payload.type)
        .setIn(['entities', payload.id, 'typeLighting'], payload.typeLighting)
        .setIn(['entities', payload.id, 'voltage'], payload.voltage)
        .setIn(['entities', payload.id, 'power'], payload.power)
        .setIn(['entities', payload.id, 'sizes'], payload.sizes);

    case EDIT_ITEM_ERROR:
      return state.set('error', error);

    case DELETE_ITEM_SUCCESS:
      return state.deleteIn(['entities', payload.id]);

    case DELETE_ITEM_ERROR:
      return state.set('error', error);

    case UPLOAD_ITEM_IMG_SUCCESS:
      return state.setIn(['entities', payload.id, 'images'], payload.images);

    case UPLOAD_ITEM_IMG_ERROR:
      return state.set('error', error);

    case EDIT_ITEM_IMG_SUCCESS:
    case DELETE_ITEM_IMG_SUCCESS:
      return state
        .setIn(['entities', payload.productId, 'images'], payload.images);

    case EDIT_ITEM_IMG_ERROR:
    case DELETE_ITEM_IMG_ERROR:
      return state.set('error', error);

    default:
      return state;
  }
}


// Selectors
export const stateSelector = state => state[moduleName];
export const productsLoadingSelector = createSelector(
  stateSelector,
  state => state.loading,
);
export const productsLoadedSelector = createSelector(
  stateSelector,
  state => state.loaded,
);
export const productsItemsSelector = createSelector(
  stateSelector,
  state => state.entities.valueSeq().toArray(),
);
export const idSelector = (_, id) => id;
export const productSelector = createSelector(
  stateSelector,
  idSelector,
  (state, id) => state.getIn(['entities', id]),
);

export const productImagesSelector = createSelector(
  stateSelector,
  idSelector,
  (state, id) => state.getIn(['entities', id, 'images']),
);


// Action Creators

export const loadAllProducts = () => (
  async (dispatch) => {
    dispatch({
      type: LOAD_ALL_START,
    });

    try {
      const productsItems = await firebase
        .firestore()
        .collection('products')
        .get()
        .then(res => res.docs.map(doc => ({
          ...doc.data(),
          id: doc.id,
        })));
      dispatch({
        type: LOAD_ALL_SUCCESS,
        payload: { productsItems },
      });
    } catch (error) {
      dispatch({
        type: LOAD_ALL_ERROR,
        error,
      });
    }
  }
);

export const addProductItem = productData => (
  async (dispatch) => {
    dispatch({
      type: ADD_ITEM_REQUEST,
    });

    try {
      await firebase
        .firestore()
        .collection('products')
        .add(productData);
      dispatch({
        type: ADD_ITEM_SUCCESS,
        payload: productData,
      });

      try {
        const productsItems = await firebase
          .firestore()
          .collection('products')
          .get()
          .then(res => res.docs.map(doc => ({
            ...doc.data(),
            id: doc.id,
          })));
        dispatch({
          type: LOAD_ALL_SUCCESS,
          payload: { productsItems },
        });

        dispatch(push('/admin/products'));
      } catch (error) {
        dispatch({
          type: LOAD_ALL_ERROR,
          error,
        });
      }
    } catch (error) {
      dispatch({
        type: ADD_ITEM_ERROR,
        error,
      });
    }
  }
);

export const editProductItem = productData => (
  async (dispatch) => {
    dispatch({
      type: EDIT_ITEM_REQUEST,
    });

    const { id, ...product } = productData;

    try {
      await firebase
        .firestore()
        .collection('products')
        .doc(id)
        .update(product);
      dispatch(push('/admin/products'));
      dispatch({
        type: EDIT_ITEM_SUCCESS,
        payload: productData,
      });
    } catch (error) {
      dispatch({
        type: EDIT_ITEM_ERROR,
        error,
      });
    }
  }
);

export const deleteProductItem = id => (
  async (dispatch, getState) => {
    dispatch({
      type: DELETE_ITEM_REQUEST,
    });

    try {
      await firebase
        .firestore()
        .collection('products')
        .doc(id)
        .delete();

      const images = productSelector(getState(), id).get('images');
      for (let i = 0; i < images.length; i += 1) {
        const image = images[i];

        const desertRef = firebase
          .storage()
          .ref()
          .child(image.pathImg);

        // Delete the file
        desertRef.delete().then(async () => {
          // console.log(`${image.pathImg} deleted!`);
        }).catch((error) => {
          dispatch({
            type: DELETE_ITEM_IMG_ERROR,
            error,
          });
        });
      }

      dispatch({
        type: DELETE_ITEM_SUCCESS,
        payload: { id },
      });
    } catch (error) {
      dispatch({
        type: DELETE_ITEM_ERROR,
        error,
      });
    }
  }
);

export const uploadImageProduct = (productId, file, order = 1) => (
  (dispatch, getState) => {
    dispatch({
      type: UPLOAD_ITEM_IMG_REQUEST,
    });

    const pathImg = `${productId}/${file.name}`;
    const storageRef = firebase.storage().ref(pathImg);

    const uploadTask = storageRef.put(file);

    uploadTask.on('state_changed',
      null,
      (error) => {
        dispatch({
          type: UPLOAD_ITEM_IMG_ERROR,
          error,
        });
      },
      () => {
        uploadTask.snapshot.ref.getDownloadURL().then(async (url) => {
          const id = Date.now();
          const images = productSelector(getState(), productId).get('images');
          const newImages = images
            .concat({
              id,
              order,
              url,
              pathImg,
            })
            .sort((a, b) => {
              if (a.order > b.order) return 1;
              if (a.order < b.order) return -1;
              return 0;
            });

          try {
            await firebase
              .firestore()
              .collection('products')
              .doc(productId)
              .update({ images: newImages });

            dispatch({
              type: UPLOAD_ITEM_IMG_SUCCESS,
              payload: { id: productId, images: newImages },
            });
          } catch (error) {
            dispatch({
              type: UPLOAD_ITEM_IMG_ERROR,
              error,
            });
          }
        });
      });
  }
);

export const editImageProduct = (productId, imgId, imgOrder) => (
  async (dispatch, getState) => {
    dispatch({
      type: EDIT_ITEM_IMG_REQUEST,
    });

    const images = productSelector(getState(), productId).get('images');
    const newImages = images
      .map((image) => {
        if (image.id === imgId) {
          return { ...image, order: imgOrder };
        }
        return image;
      })
      .sort((a, b) => {
        if (a.order > b.order) return 1;
        if (a.order < b.order) return -1;
        return 0;
      });

    try {
      await firebase
        .firestore()
        .collection('products')
        .doc(productId)
        .update({ images: newImages });
      dispatch({
        type: EDIT_ITEM_IMG_SUCCESS,
        payload: { productId, images: newImages },
      });
    } catch (error) {
      dispatch({
        type: EDIT_ITEM_IMG_ERROR,
        error,
      });
    }
  }
);

export const deleteImageProduct = (productId, imgId) => (
  (dispatch, getState) => {
    dispatch({
      type: DELETE_ITEM_IMG_REQUEST,
    });

    const images = productSelector(getState(), productId).get('images');
    const newImages = images.filter(image => (image.id !== imgId));

    const desertRef = firebase
      .storage()
      .ref()
      .child(images.find(image => (image.id === imgId)).pathImg);

    // Delete the file
    desertRef.delete().then(async () => {
      try {
        await firebase
          .firestore()
          .collection('products')
          .doc(productId)
          .update({ images: newImages });
        dispatch({
          type: DELETE_ITEM_IMG_SUCCESS,
          payload: { productId, images: newImages },
        });
      } catch (error) {
        dispatch({
          type: DELETE_ITEM_IMG_ERROR,
          error,
        });
      }
    }).catch((error) => {
      dispatch({
        type: DELETE_ITEM_IMG_ERROR,
        error,
      });
    });
  }
);
