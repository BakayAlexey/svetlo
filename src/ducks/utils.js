import { Map } from 'immutable';

export function fbToEntities(values, DataRecord) {
  return new Map(
    values.map(value => ([value.id, new DataRecord(value)])),
  );
}
