import firebase from 'firebase/app';
import 'firebase/firestore';
import { Record, Map } from 'immutable';
import { createSelector } from 'reselect';
import { push } from 'connected-react-router';
import { appName } from '../config';
import { fbToEntities } from './utils';


// Constants

export const moduleName = 'faq';
const prefix = `${appName}/${moduleName}`;

const LOAD_ALL_START = `${prefix}/LOAD_ALL_START`;
const LOAD_ALL_SUCCESS = `${prefix}/LOAD_ALL_SUCCESS`;
const LOAD_ALL_ERROR = `${prefix}/LOAD_ALL_ERROR`;

const ADD_ITEM_REQUEST = `${prefix}/ADD_ITEM_START`;
const ADD_ITEM_SUCCESS = `${prefix}/ADD_ITEM_SUCCESS`;
const ADD_ITEM_ERROR = `${prefix}/ADD_ITEM_ERROR`;

const EDIT_ITEM_REQUEST = `${prefix}/EDIT_ITEM_START`;
const EDIT_ITEM_SUCCESS = `${prefix}/EDIT_ITEM_SUCCESS`;
const EDIT_ITEM_ERROR = `${prefix}/EDIT_ITEM_ERROR`;

const DELETE_ITEM_REQUEST = `${prefix}/DELETE_ITEM_START`;
const DELETE_ITEM_SUCCESS = `${prefix}/DELETE_ITEM_SUCCESS`;
const DELETE_ITEM_ERROR = `${prefix}/DELETE_ITEM_ERROR`;


// Reducer

const FaqRecord = Record({
  id: null,
  order: null,
  question: null,
  answer: null,
});

const ReducerState = Record({
  entities: new Map({}),
  loading: false,
  loaded: false,
  error: null,
});

export default function reducer(state = new ReducerState(), action) {
  const { type, payload, error } = action;

  switch (type) {
    case LOAD_ALL_START:
      return state.set('loading', true);

    case LOAD_ALL_SUCCESS:
      return state
        .set('loading', false)
        .set('loaded', true)
        .set('entities', fbToEntities(payload.faqItems, FaqRecord));

    case LOAD_ALL_ERROR:
      return state.set('error', error);

    case ADD_ITEM_REQUEST:
    case ADD_ITEM_SUCCESS:
    case ADD_ITEM_ERROR:
      return state.set('error', error);

    case EDIT_ITEM_SUCCESS:
      return state
        .setIn(['entities', payload.id, 'order'], payload.order)
        .setIn(['entities', payload.id, 'question'], payload.question)
        .setIn(['entities', payload.id, 'answer'], payload.answer);

    case EDIT_ITEM_ERROR:
      return state.set('error', error);

    case DELETE_ITEM_SUCCESS:
      return state.deleteIn(['entities', payload.id]);

    case DELETE_ITEM_ERROR:
      return state.set('error', error);

    default:
      return state;
  }
}

// Selectors
export const stateSelector = state => state[moduleName];
export const faqLoadingSelector = createSelector(
  stateSelector,
  state => state.loading,
);
export const faqLoadedSelector = createSelector(
  stateSelector,
  state => state.loaded,
);
export const faqItemsSelector = createSelector(
  stateSelector,
  state => state.entities.valueSeq().toArray(),
);
export const idSelector = (_, id) => id;
export const faqSelector = createSelector(
  stateSelector,
  idSelector,
  (state, id) => state.getIn(['entities', id]),
);


// Action Creators

export const loadAllFaq = () => (
  async (dispatch) => {
    dispatch({
      type: LOAD_ALL_START,
    });

    try {
      const faqItems = await firebase
        .firestore()
        .collection('faq')
        .get()
        .then(res => res.docs.map(doc => ({
          ...doc.data(),
          id: doc.id,
        })));
      dispatch({
        type: LOAD_ALL_SUCCESS,
        payload: { faqItems },
      });
    } catch (error) {
      dispatch({
        type: LOAD_ALL_ERROR,
        error,
      });
    }
  }
);

export const addFaqItem = (order, question, answer) => (
  async (dispatch) => {
    dispatch({
      type: ADD_ITEM_REQUEST,
      payload: { order, question, answer },
    });

    try {
      await firebase.firestore().collection('faq').add({ order, question, answer });
      dispatch({
        type: ADD_ITEM_SUCCESS,
        payload: { order, question, answer },
      });

      try {
        const faqItems = await firebase.firestore().collection('faq').get().then(res => res.docs.map(doc => ({
          ...doc.data(),
          id: doc.id,
        })));
        dispatch({
          type: LOAD_ALL_SUCCESS,
          payload: { faqItems },
        });

        dispatch(push('/admin/faq'));
      } catch (error) {
        dispatch({
          type: LOAD_ALL_ERROR,
          error,
        });
      }
    } catch (error) {
      dispatch({
        type: ADD_ITEM_ERROR,
        error,
      });
    }
  }
);

export const editFaqItem = (id, order, question, answer) => (
  async (dispatch) => {
    dispatch({
      type: EDIT_ITEM_REQUEST,
      payload: {
        id,
        order,
        question,
        answer,
      },
    });

    try {
      await firebase
        .firestore()
        .collection('faq')
        .doc(id)
        .update({ order, question, answer });
      dispatch({
        type: EDIT_ITEM_SUCCESS,
        payload: {
          id,
          order,
          question,
          answer,
        },
      });
      dispatch(push('/admin/faq'));
    } catch (error) {
      dispatch({
        type: EDIT_ITEM_ERROR,
        error,
      });
    }
  }
);

export const deleteFaqItem = id => (
  async (dispatch) => {
    dispatch({
      type: DELETE_ITEM_REQUEST,
      payload: { id },
    });

    try {
      await firebase
        .firestore()
        .collection('faq')
        .doc(id)
        .delete();
      dispatch({
        type: DELETE_ITEM_SUCCESS,
        payload: { id },
      });
    } catch (error) {
      dispatch({
        type: DELETE_ITEM_ERROR,
        error,
      });
    }
  }
);
