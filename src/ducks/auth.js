import { Record } from 'immutable';
import firebase from 'firebase/app';
import 'firebase/auth';
import { createSelector } from 'reselect';
import { appName } from '../config';

// Constants

export const moduleName = 'auth';
const prefix = `${appName}/${moduleName}`;

const SIGN_IN_START = `${prefix}/SIGN_IN_START`;
const SIGN_IN_SUCCESS = `${prefix}/SIGN_IN_SUCCESS`;
const SIGN_IN_ERROR = `${prefix}/SIGN_IN_ERROR`;
const SIGN_OUT_START = `${prefix}/SIGN_OUT_START`;
const SIGN_OUT_SUCCESS = `${prefix}/SIGN_OUT_SUCCESS`;
const SIGN_OUT_ERROR = `${prefix}/SIGN_OUT_ERROR`;


// Reducer

const ReducerRecord = Record({
  user: null,
  error: null,
});

export default function reducer(state = new ReducerRecord(), action) {
  const { type, payload, error } = action;

  switch (type) {
    case SIGN_IN_SUCCESS:
      return state.set('user', payload.user);
    case SIGN_IN_ERROR:
      return state.set('error', error);
    case SIGN_OUT_SUCCESS:
      return state.set('user', null);
    case SIGN_OUT_ERROR:
      return state.set('error', error);
    default:
      return state;
  }
}


// Selectors

export const userSelector = state => state[moduleName].user;
export const isAuthorizedSelector = createSelector(
  userSelector,
  user => !!user,
);


// Action Creators

export function signIn(email, password) {
  return async (dispatch) => {
    dispatch({
      type: SIGN_IN_START,
    });

    try {
      const user = await firebase.auth().signInWithEmailAndPassword(email, password);

      dispatch({
        type: SIGN_IN_SUCCESS,
        payload: { user },
      });
    } catch (error) {
      dispatch({
        type: SIGN_IN_ERROR,
        error,
      });
    }
  };
}

export function signOut() {
  return async (dispatch) => {
    dispatch({
      type: SIGN_OUT_START,
    });

    try {
      await firebase.auth().signOut();
      dispatch({
        type: SIGN_OUT_SUCCESS,
      });
    } catch (error) {
      dispatch({
        type: SIGN_OUT_ERROR,
        error,
      });
    }
  };
}

export function initAuth() {
  return async (dispatch) => {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        dispatch({
          type: SIGN_IN_SUCCESS,
          payload: { user },
        });
      }
    });
  };
}
