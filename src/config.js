import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';

export const appName = 'svetlo-react';

const firebaseConfig = {
  apiKey: 'AIzaSyDo-01ic0popfQ9RgZdFNqVQoqQ_MF8myY',
  authDomain: `${appName}.firebaseapp.com`,
  databaseURL: 'https://svetlo-react.firebaseio.com',
  projectId: appName,
  storageBucket: `${appName}.appspot.com`,
  messagingSenderId: '890820538058',
  appId: '1:890820538058:web:40748c6cc9de34cf',
};

firebase.initializeApp(firebaseConfig);
