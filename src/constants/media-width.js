export const exxSmallWidths = 'screen and (max-width: 479px)';
export const extraSmallWidths = 'screen and (max-width: 767px)';
export const smallWidths = 'screen and (max-width: 1023px)';
export const mediumWidths = 'screen and (max-width: 1199px)';
export const largeWidths = 'screen and (max-width: 1399px)';
