import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import history from '../history';
import auth, { moduleName as authModule } from '../ducks/auth';
import faq, { moduleName as faqModule } from '../ducks/faq';
import products, { moduleName as productsModule } from '../ducks/products';

export default combineReducers({
  router: connectRouter(history),
  [authModule]: auth,
  [faqModule]: faq,
  [productsModule]: products,
});
